#!/bin/bash 

docker run -it -d --rm --name runner --net=host \
 --add-host gitlab.di0703.com:192.168.11.131 \
 --add-host registry.di0703.com:192.168.11.131  \
 -v ${PWD}/config/config.toml:/etc/gitlab-runner/config.toml \
 -v ${PWD}/config/ca.crt:/etc/gitlab-runner/certs/ca.crt \
 -e "CA_CERTIFICATES_PATH=/etc/gitlab-runner/certs/ca.crt" \
 gitlab/gitlab-runner && docker logs runner -f 
